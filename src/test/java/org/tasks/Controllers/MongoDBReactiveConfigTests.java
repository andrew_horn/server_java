package org.tasks.Controllers;

import org.springframework.boot.autoconfigure.mongo.MongoProperties;

import java.lang.reflect.Method;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


//TODO Тест устарел. Поправить

public class MongoDBReactiveConfigTests {

    @Test
    public void mongoPropertiesTest() {

        try {

            MongoTemplateReactiveConfig mongoDBReactiveConfig = new MongoTemplateReactiveConfig();

            Method mongoPropertiesMethod = mongoDBReactiveConfig.getClass()
                    .getDeclaredMethod("mongoProperties");

            mongoPropertiesMethod.setAccessible(true);

            MongoProperties mongoProperties = (MongoProperties) mongoPropertiesMethod.invoke(mongoDBReactiveConfig);


            System.out.println("db: " + mongoProperties.getDatabase());

            assertEquals(mongoProperties.getDatabase(), "tasks_db_20240810");

        } catch (Exception exception) {
//            TODO сделать лог
            System.out.println("тест подключения к базе данных закончился неудачей");
        }
    }
}
