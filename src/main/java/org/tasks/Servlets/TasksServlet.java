package org.tasks.Servlets;

import com.google.gson.Gson;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.tasks.Controllers.MongoTemplateConfig;
import org.tasks.Models.ITask;


@WebServlet(urlPatterns = {"/tasks"}, asyncSupported = true)
public class TasksServlet extends HttpServlet {

    private static final MongoOperations mongoOperations =
            new MongoTemplateConfig().mongoTemplate();

    private static final Logger logger = LoggerFactory.getLogger(TasksServlet.class);


    @Override
    public void init() {

        logger.trace("запущен {}", TasksServlet.class.getSimpleName());

    }

    //  TODO обработка исключений
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        ITask[] taskArray = mongoOperations.findAll(ITask.class).toArray(ITask[]::new);

        String taskArrayJsonString = new Gson().toJson(taskArray);
        PrintWriter printWriter = response.getWriter();

        //        TODO для работы CORSE убрать заголовок "*"
        response.addHeader("Access-Control-Allow-Origin", "*");
        printWriter.print(taskArrayJsonString);
        printWriter.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        //        TODO для работы CORSE убрать заголовок "*"
        response.setHeader("Access-Control-Allow-Origin", "*");

        AsyncContext asyncContext = request.startAsync();
        asyncContext.addListener(new AsyncListener() {

            @Override
            public void onComplete(AsyncEvent asyncEvent) throws IOException {

                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletRequest request = (HttpServletRequest) asyncContext.getRequest();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();

                ITask taskRequest = new Gson().fromJson(request.getReader(), ITask.class);
                ITask taskResponse = mongoOperations.insert(taskRequest);

                printWriter.print(new Gson().toJson(taskResponse));
                response.setStatus(HttpServletResponse.SC_OK);
                logger.trace("в БД добавлена задача {} с ID {}", taskResponse.getTaskName(), taskResponse.getId());
                printWriter.flush();

            }

            @Override
            public void onTimeout(AsyncEvent asyncEvent) throws IOException {

                logger.debug("{} doPost, превышено время ожидания записи в БД",
                        TasksServlet.class.getSimpleName());
                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                printWriter.print(new Gson().toJson("Превышено время ожидания записи в БД"));
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
            }

            @Override
            public void onError(AsyncEvent asyncEvent) throws IOException {

                logger.error("{} doPost, ошибка записи в БД",
                        TasksServlet.class.getSimpleName());
                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                printWriter.print(new Gson().toJson("Ошибка записи в БД"));
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);

            }

            @Override
            public void onStartAsync(AsyncEvent asyncEvent) {

                logger.trace("запущен {} doPost onStartAsync",
                        TasksServlet.class.getSimpleName());

            }
        });
    }

    @Override
    protected void doPatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //        TODO для работы CORSE убрать заголовок "*"
        response.setHeader("Access-Control-Allow-Origin", "*");

        AsyncContext asyncContext = request.startAsync();
        asyncContext.addListener(new AsyncListener() {

            @Override
            public void onComplete(AsyncEvent asyncEvent) throws IOException {

                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletRequest request = (HttpServletRequest) asyncContext.getRequest();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();

                ITask taskRequest = new Gson().fromJson(request.getReader(), ITask.class);

                Query updateQuery = new Query().addCriteria(Criteria.where("_id")
                        .is(taskRequest.getId()));

                FindAndReplaceOptions options = new FindAndReplaceOptions().returnNew();

                ITask taskResponse = mongoOperations.
                        findAndReplace(updateQuery, taskRequest, options);

                if (taskResponse != null) {
                    printWriter.print(new Gson().toJson(taskResponse));
                    response.setStatus(HttpServletResponse.SC_OK);
                    logger.trace("задача {} с ID {} обновлена", taskResponse.getTaskName(), taskResponse.getId());
                    printWriter.flush();

                }
                printWriter.print(new Gson().toJson("Задача " + taskRequest.getTaskName() + " c ID "
                        + taskRequest.getId() + " в БД не найдена"));
                response.setStatus(HttpServletResponse.SC_OK);
                logger.trace("задача {} с ID {} в БД не найдена", taskRequest.getTaskName(), taskRequest.getId());
                printWriter.flush();

            }

            @Override
            public void onTimeout(AsyncEvent asyncEvent) throws IOException {

                logger.debug("{} doPatch, превышено время ожидания записи в БД",
                        TasksServlet.class.getSimpleName());
                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                printWriter.print(new Gson().toJson("Превышено время ожидания записи в БД"));
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
            }

            @Override
            public void onError(AsyncEvent asyncEvent) throws IOException {

                logger.error("{} doPatch, ошибка записи в БД",
                        TasksServlet.class.getSimpleName());
                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                printWriter.print(new Gson().toJson("Ошибка записи в БД"));
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);

            }

            @Override
            public void onStartAsync(AsyncEvent asyncEvent) {

                logger.trace("запущен {} doPatch onStartAsync",
                        TasksServlet.class.getSimpleName());

            }
        });

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //        TODO для работы CORSE убрать заголовок "*"
        response.setHeader("Access-Control-Allow-Origin", "*");

        AsyncContext asyncContext = request.startAsync();
        asyncContext.addListener(new AsyncListener() {

            @Override
            public void onComplete(AsyncEvent asyncEvent) throws IOException {

                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletRequest request = (HttpServletRequest) asyncContext.getRequest();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                ITask taskRequest = new Gson().fromJson(request.getReader(), ITask.class);

                mongoOperations.remove(taskRequest);
                printWriter.print(new Gson().toJson("Задача " + taskRequest.getTaskName()
                        + " ID " + taskRequest.getId() + " удалена из БД"));
                response.setStatus(HttpServletResponse.SC_OK);
                logger.trace("задача {} с ID {} удалена", taskRequest.getTaskName(), taskRequest.getId());
                printWriter.flush();

            }

            @Override
            public void onTimeout(AsyncEvent asyncEvent) throws IOException {

                logger.debug("{} doDelete, превышено время ожидания записи в mongodb",
                        TasksServlet.class.getSimpleName());
                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                printWriter.print(new Gson().toJson("Превышено время ожидания записи в БД"));
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
            }

            @Override
            public void onError(AsyncEvent asyncEvent) throws IOException {

                logger.error("{} doDelete, ошибка записи в mongodb",
                        TasksServlet.class.getSimpleName());
                AsyncContext asyncContext = asyncEvent.getAsyncContext();
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                PrintWriter printWriter = asyncContext.getResponse().getWriter();
                printWriter.print(new Gson().toJson("Ошибка записи в БД"));
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);

            }

            @Override
            public void onStartAsync(AsyncEvent asyncEvent) throws IOException {

                logger.trace("запущен {} doDelete onStartAsync",
                        TasksServlet.class.getSimpleName());

            }
        });

    }

    @Override
    protected void doOptions(HttpServletRequest request, HttpServletResponse response) {

        String clientOrigin = request.getHeader("origin");

        response.setHeader("Access-Control-Allow-Origin", clientOrigin);
        response.setHeader("Access-Control-Allow-Headers", "*");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PATCH, OPTIONS");
        response.setStatus(HttpServletResponse.SC_OK);
    }


    //    TODO Убрать после отладки
    public Date dateParcer(String stringDate) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateDate = null;

        try {
            dateDate = simpleDateFormat.parse(stringDate);
//            System.out.println(dateDate);
        } catch (Exception e) {
            System.out.println("Не удалось преобразовать строку в дату, " + e);
        }

        return dateDate;
    }

}
