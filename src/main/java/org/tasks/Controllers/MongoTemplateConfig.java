package org.tasks.Controllers;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class MongoTemplateConfig {

    @Bean
    @NotNull
    private MongoDatabaseFactory mongoDatabaseFactory() {

        MongoClientConfig mongoClientConfig = MongoClientConfig.getInstance();
        return new SimpleMongoClientDatabaseFactory(mongoClientConfig.mongoClient, mongoClientConfig.mongoProperties.getDatabase());
    }

    @Bean
    @NotNull
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoDatabaseFactory());
    }
}
