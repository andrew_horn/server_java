package org.tasks.Controllers;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;

import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Properties;

/**
 * Клиент локальной базы данных MongoDB.
 * Для настройки используются только параметры:
 * host, port, database, указанные в
 * src/main/resources/application.properties,
 * если параметры в файле не найдены, используются параметры
 * по умолчанию: localhost, 27017, tasks_db
 * подключение к MongoDB производится без авторизации
 */

//    TODO mongoClient() должен быть singleton
//    TODO добавить логгер

@Configuration
public class MongoClientReactiveConfig {

    public MongoProperties mongoProperties;
    public MongoClient mongoClient;

    private static final MongoClientReactiveConfig mongoClientConfig = new MongoClientReactiveConfig();

    private MongoClientReactiveConfig() {
        this.mongoPropertiesSet();
        this.mongoClientSet();
    }

    public static MongoClientReactiveConfig getInstance() {
        return mongoClientConfig;
    }

    @Bean
    private void mongoPropertiesSet() {

        this.mongoProperties = new MongoProperties();
        Properties properties = new Properties();
        String mongoConfigConnectionMessage = "";

//        TODO настроить получение абсолютного пути
        try (FileInputStream fileInputStream = new FileInputStream(
                "/home/horn/hdd/projects/server_java/" +
                        "src/main/resources/application.properties")) {

            properties.load(fileInputStream);

            if (properties.getProperty("mongodb.host") != null)
                mongoProperties.setHost(properties.getProperty("mongodb.host"));
            else {
                mongoProperties.setHost("localhost");
                mongoConfigConnectionMessage = "(установлен по умолчанию) ";
            }
            mongoConfigConnectionMessage += "host: " + mongoProperties.getHost() + "\n";

            if (properties.getProperty("mongodb.port") != null)
                mongoProperties.setPort(Integer.valueOf(properties.getProperty("mongodb.port")));
            else {
                mongoProperties.setPort(27017);
                mongoConfigConnectionMessage += "(установлен по умолчанию) ";
            }
            mongoConfigConnectionMessage += "port: " + mongoProperties.getPort() + "\n";

            if (properties.getProperty("mongodb.database") != null)
                mongoProperties.setDatabase(properties.getProperty("mongodb.database"));
            else {
                mongoProperties.setDatabase("tasks_db");
                mongoConfigConnectionMessage += "(установлен по умолчанию) ";
            }
            mongoConfigConnectionMessage += "database: " + mongoProperties.getDatabase() + "\n";

        } catch (IOException error) {
            mongoProperties.setHost("localhost");
            mongoProperties.setPort(27017);
            mongoProperties.setDatabase("tasks_db");
            mongoConfigConnectionMessage = "Не удалось прочитать файл с настройками подключения к mongodb,\n" +
                    error + "\n установлены настройки mongodb по умолчанию\n" +
                    "host: " + mongoProperties.getHost() + "\n" +
                    "port: " + mongoProperties.getPort() + "\n" +
                    "database: " + mongoProperties.getDatabase();

        } finally {
            //    TODO вывод параметров подключения к mongodb в лог
            System.out.println(mongoConfigConnectionMessage);
        }
    }

    @Bean
    private void mongoClientSet() {
        this.mongoClient = MongoClients.create(MongoClientSettings.builder()
                .applyToClusterSettings(builder -> {
                            builder.hosts(
                                    Collections.singletonList(new ServerAddress(
                                                    this.mongoProperties.getHost(),
                                                    this.mongoProperties.getPort()
                                            )
                                    )
                            );
                        }
                ).build());
    }

}
