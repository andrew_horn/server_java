package org.tasks.Controllers;

import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory;

import org.springframework.context.annotation.Bean;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;

/**
 * Настройка реактивного подключения
 * модели данных к клиенту MongoDB.
 */


@Configuration
public class MongoTemplateReactiveConfig {

    @Bean
    @NotNull
    private ReactiveMongoDatabaseFactory reactiveMongoDatabaseFactory() {

        MongoClientReactiveConfig mongoClientConfig = MongoClientReactiveConfig.getInstance();
        return new SimpleReactiveMongoDatabaseFactory(mongoClientConfig.mongoClient,
                mongoClientConfig.mongoProperties.getDatabase());
    }

    @Bean
    @NotNull
    public ReactiveMongoTemplate reactiveMongoTemplate() {
        return new ReactiveMongoTemplate(reactiveMongoDatabaseFactory());
    }
}