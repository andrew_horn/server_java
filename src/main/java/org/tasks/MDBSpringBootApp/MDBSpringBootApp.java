package org.tasks.MDBSpringBootApp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.data.mongodb.core.*;

import org.tasks.Controllers.MongoTemplateReactiveConfig;
import org.tasks.Models.ITask;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@SpringBootApplication
@EnableMongoRepositories
public class MDBSpringBootApp implements CommandLineRunner {

    public MDBSpringBootApp() throws IOException {
    }

    public static void main(String[] args) {
        SpringApplication.run(MDBSpringBootApp.class, args);
    }

    ReactiveMongoOperations reactiveMongoOperations = new MongoTemplateReactiveConfig()
            .reactiveMongoTemplate();


    void createTasksItems() {
        System.out.println("Data creation started...");

        reactiveMongoOperations
                .insert(new ITask("помыть пол", new String[]{"Мария", "Екатерина"},
                        dateParcer("2024-01-15 00:00:00"), dateParcer("2024-01-16 00:00:00"),
                        0, 1))
                .subscribe();
        reactiveMongoOperations
                .insert(new ITask("подстричь газон", new String[]{"Петр"},
                        dateParcer("2024-01-18 00:00:00"), dateParcer("2024-01-19 00:00:00"),
                        0, 2))
                .subscribe();
        reactiveMongoOperations
                .insert(new ITask("затариться в магазине", new String[]{"Федул", "Фекла"},
                        dateParcer("2024-01-20 00:00:00"), dateParcer("2024-01-21 00:00:00"),
                        0, 1))
                .subscribe();

        System.out.println("Data creation complete...");
    }

    @Override
    public void run(String... args) throws Exception {


        System.out.println("Старт...");

        createTasksItems();

        System.out.println("Конец...");
    }

    public Date dateParcer(String stringDate) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateDate = null;

        try {
            dateDate = simpleDateFormat.parse(stringDate);
            System.out.println(dateDate);
        } catch (Exception e) {
            System.out.println("Не удалось преобразовать строку в дату, " + e);
        }

        return dateDate;
    }
}

