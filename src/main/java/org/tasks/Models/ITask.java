package org.tasks.Models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Document
public class ITask {

    //    TODO название задачи
    private String taskName;

    //    TODO массив исполнителей задач
    private String[] executors;

    //    TODO дата создания задачи, берется текущее системное время
    private Date create;

    //    TODO дата начала выполнения задачи, задется пользователем
    private Date start;

    //    TODO срок выполнения задачи, задается пользователем
    private Date end;

    //    TODO статус выполнения задачи: 0 - не начиналась, 1 - выполняется, 2 - завершена
    private int status;

    //    TODO приоритет: 0- низкий, 1 - средний, 2 - высокий
    private int priority;

    //    TODO ID задачи берется из mongodb после создания записи
    @Id
    private String id;

    //    TODO убрать после отладки
    public ITask() {

    }

    public ITask(String taskName, String[] executors, Date start, Date end, int status, int priority) {
        this.taskName = taskName;
        this.executors = executors;
        this.start = start;
        this.end = end;
        this.status = status;
        this.priority = priority;
//        TODO возможно дату создания следует брать с фронта,
//         т.к. сервер может быть в другом часовом поясе
        setCreate();
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setExecutors(String[] executors) {
        this.executors = executors;
    }

    public String[] getExecutors() {
        return executors;
    }

    public void setCreate() {
        this.create = new Date();
    }

    public Date getCreate() {
        return create;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStart() {
        return start;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getEnd() {
        return end;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public String getId() {
        return id;
    }
}
